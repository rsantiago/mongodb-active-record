<?php
	
class MongoFactory
{

	public static $instance;

	public $db;

	public function __construct(){

	}
	public static function getInstance(){
		if(self::$instance == null):

			self::$instance = new Mongo( 'mongodb://test:test@ds029187.mongolab.com:29187/' );

			$this->setDB( 'pedidos' );

			return self::$instance;
		else:
			return self::$instance;
		endif;
	}

	private function setDB( $db ){
		$this->db = self::$instance->selectDB( $db );
	}	
}